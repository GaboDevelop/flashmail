<?php
    function buscarDireccion($direcciones,$orden){                   
        foreach($direcciones as $direccion){
                    
            if($direccion->id ==  $orden){
                $direccion1 = $direccion->ciudad;
                break;
            }
            
        }
        return $direccion1;
    }  
?>


<div class="row">
     <div class="offset-md-11">
         <button action="#" id="cerrar" class="btn botonOrden" onclick="mostrarPedido()">X</button>
     </div>
 </div>

    
    <div class="row">
        <div class="col-md-10 offset-md-1">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#ORDEN</th>
                    <th scope="col">NOMBRE DESTINARIO</th>
                    <th scope="col">APELLDIO DESTINARIO</th>
                    <th scope="col">DIRECCION DE ENVIO</th>
                    <th scope="col">DIRECCION DESTINO</th>
                    <th scope="col">CONTENIDO</th>
                    <th scope="col">KG</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                    $ordenes = \flashmail\Orden::All();
                    $direcciones = \flashmail\Direccion::All();
                    $paquetes = \flashmail\Paquete::All();
                    foreach($ordenes as $orden){
                        if($orden->cliente_id ==  $cliente->id){
                            $direccionOrigen = buscarDireccion($direcciones,$orden->direccion_recolectado);
                            $direccionDestino = buscarDireccion($direcciones,$orden->direccion_entrega);
                            
                            foreach($paquetes as $paquete){
                                if($orden->id == $paquete->orden_id){
                    ?>
                                    <tr>
                                        <td>{{$orden->id}}</td>
                                        <td>{{$orden->nombre_destinatario}}</td>
                                        <td>{{$orden->apellido_destinatario}}</td>
                                        <td>{{$direccionOrigen}}</td>
                                       
                                        <td>{{$paquete->contenido}}</td>
                                        <td>{{$paquete->peso}}</td>
                                    </tr>
                <?php
                                }
                            }
                        }
                    }
                ?>
                        
               
            </tbody>
            
            
        </table>

        </div>

    </div>
 