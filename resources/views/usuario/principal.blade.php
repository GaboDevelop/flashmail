@extends('plantilla.plantilla')

@section('seccion')




    
    <?php
        $clientes = \flashmail\Cliente::All();
        foreach($clientes as $cliente){
            if($cliente->usuario_id ==  Auth::user()->id){
                break;
            }
        }
        $direcciones = \flashmail\Direccion::All();
        foreach($direcciones as $direccion){
            if($direccion->cliente_id ==  $cliente->id){
                break;
            }
        }

        

    ?>

     <!-- INCIO: RUTAS UTILIZADAS PARA EL MAPA-->
    @include('rutas.mapa')
     <!-- FIN: RUTAS UTILZIADAS PARA EL MAPA-->


    <header class="position-relative">
        <div class="container-fluid">
            <div class="row">
                <nav class="navbar col-md-12">
                    <div class="container-fluid">
                            <div class="logo col-md-2">
                                    <img src="{{asset('Imagenes/logo.png')}}" alt="" height="50">
                                    </div>
                            <div class="col-md-2 offset-md-8 text-center">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-fw fa-user-circle"></i> {{$cliente->nombre." ".$cliente->apellido}}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="/logout">Cerrar Sesión</a>
                                    </div>
                                </div>
                               
                            </div>
                    </div>
                        
                    
                </nav>
            </div>
        </div>
    </header>

    


    <section class="menuVertical">
      <div class="container-fluid">
        <div class="row">

            <div class="left position-relative text-center menuLateral" id="menuLateral">
                <h6>
                    <div>
                        <i class="fas fa-fw fa-bars"></i>Menú
                    </div>
                </h6>
                <a href="#" id="miPerfil" onclick="mostrarPerfil()">
                    <div class="item">
                        <i class="fas fa-fw fa-user-circle"></i> Mi perfil
                    </div>
                </a>
                <a href="#" id="paqueteNuevo" onclick="mostrarPedido()">
                    <div class="item active">
                        <i class="fas fa-fw fa-map-marked-alt"></i> Nueva Orden
                    </div>
                </a>
                <a href="#">
                    <div class="item">
                        <i class="fas fa-fw fa-columns"></i> Mis Ordenes
                    </div>
                </a>
                <a href="#">
                    <div class="item">
                        <i class="fas fa-fw fa-trash"></i> Eliminar Orden
                    </div>
                </a>
            </div>

            
            <!-- INCIO: FORMULARIO DE MOSTRAR PERFIL-->
                @include('usuario.perfil')
            <!-- FIN: FORMULARIO DE MOSTRAR PERFIL-->
            

            
                <div class="misPaquetes position-absolute col-md-8 offset-md-2">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                @include('mapa.misPedidos')
                            </div>
                        </div>
                    </div>
                </div>



            <div class="paquete position-absolute">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- INCIO: FORMULARIO DE REGISTRO-->
                            @include('formularios.paquetes')
                             <!-- FIN : FORMULARIO DE REGISTRO-->
                        </div>
                    </div>   
                </div>
            </div>
            

            <!-- INCIO: MOSTRAR MAPA-->
               @include('mapa.mostrar')
            <!-- FIN: MOSTRAR MAPA-->
        </div>
      </div>
    </section>


