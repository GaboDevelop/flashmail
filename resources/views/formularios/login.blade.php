{!!Form::open(['route'=>'login.store','method'=>'POST'])!!}
    <div class="row">
        <div class="form-group col-md-4 offset-md-3">
            {!!Form::email('correoSesion',null,['class'=>'form-control box','id'=>'correoSesion','placeholder'=>'correo'])!!}
        </div>
        <div class="form-group col-md-4">
                {!!Form::password('contraseñaSesion',['class'=>'form-control box','id'=>'contraseñaSesion','placeholder'=>'contraseña'])!!}
        </div>
        <button type="submit" class="btn btn-start-order col-md-1 text-center botones">Entrar</button>
    </div>
{!!Form::close()!!}