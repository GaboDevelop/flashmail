
 {!!Html::script('js/ocultarMostrarOrden.js')!!}
 {!!Html::script('js/ocultarFormularioOrden.js')!!}


 <div class="row">
     <div class="offset-md-11">
         <button action="#" id="cerrar" class="btn botonOrden" onclick="mostrarPedido()">X</button>
     </div>
 </div>

{!!Form::open(['id' => 'form'])!!}
    <div id="mensajeRegistro">
        <br>
        <div class="alert alert-success alert-dismissible fade show" id="mensaje-success" role="alert" style="display:none;">
                <strong>EN CAMINO!</strong> Orden Registrada con exito(cierre este mensaje).
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="ocultarFormulario()">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>    

    <div class="alert alert-danger alert-dismissible fade show" id="mensaje-danger" role="alert" style="display:none;">
            <strong>ups!</strong> Por favor introduzca información valida.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    </div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    <div class="row">
        <div class="form-group col-md-6">
            {!!Form::label('Nombre de Destinatario:')!!}
            {!!Form::text('nombreDestinatario',null,['class'=>'form-control','placeholder'=>'Nombres','tabindex'=>'1','id'=>'nombreDestinatario','required'])!!}
        </div>
        <div class="form-group col-md-6">
            {!!Form::label('Apellido de Destinatario:')!!}
            {!!Form::text('apellidoDestinatario',null,['class'=>'form-control','placeholder'=>'Apellidos','tabindex'=>'2','id'=>'apellidoDestinatario','required'])!!}
        </div>
    </div>

    <!--<div class="row">
            <div class="form-group col-md-12">

                    {!!Form::label('Dirección de inicio:')!!}
                    <input type="search" id="direccion" placeholder="Busca la dirección" tabindex="3"  disabled="true" required>
    
                    <script>
                        placeSearch({
                            key: 'E9EKSMkLWxbRBE4NYn9P3GVHAnAmbbjd',
                            container: document.querySelector('#direccion')
                        });
                    </script>
            </div>
    </div>  -->


    <div class="row" id="cantidadPaquetes">
        <div class="form-group col-md-10">
            {!!Form::label('Contenido Paquete:')!!}
            {!!Form::text('contenidoPaquete',null,['class'=>'form-control','placeholder'=>'Contenido...','tabindex'=>'5','id'=>'contenidoPaquete1','required'])!!} 
        </div>
        <div class="form-group col-md-2">
            {!!Form::label('Peso:')!!}
            <input type="number" class="form-control" id="peso1" placeholder="KG" tabindex="6" required>
            <input type="hidden" id="clienteId" value="{{$cliente->id}}">
        </div>
    </div>
    <div class="row">
        {!!link_to('#',$title='Agregar Paquete',$attributes = ['class'=>'btn col-md-4 offset-md-7 botones', 'onclick'=>'agregarPaquetes()'],$secure = null)!!}
    </div>
     <br>
     <div class="row">
         {!!link_to('#',$title='Utilizar Mapa',$attributes = ['id'=> 'botonMapa','class'=>'btn col-md-4 offset-md-4 botones', 'onclick'=>'activarMapa()'],$secure = null)!!}
     </div>
    <br>
    <div class="from-group">
            <input type="hidden" id="paisRecolectado" value="">
            <input type="hidden" id="estadoRecolectado" value="">
            <input type="hidden" id="ciudadRecolectado" value="">
            <input type="hidden" id="zonaRecolectado" value="">
            <input type="hidden" id="codigoPostalRecolectado" value="">
            <input type="hidden" id="paisEntrega" value="">
            <input type="hidden" id="estadoEntrega" value="">
            <input type="hidden" id="ciudadEntrega" value="">
            <input type="hidden" id="zonaEntrega" value="">
            <input type="hidden" id="codigoPostalEntrega" value="">
            <input type="hidden" id="esBuscador" value="">

            <input type="hidden" id="numeroPaquetes" value="1">
    </div>
    
    <div class="row">
        <div class="col-md-4 offset-md-4">
            {!!link_to('#',$title='Registrar Orden',$attributes = ['id' =>'registro', 'class'=>'btn col-md-12 botones'],$secure = null)!!}
        </div>
    </div>      
    
{!!Form::close()!!}

