
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>FlashMail</title>

    {!!Html::style('css/reset.css')!!}
    {!!Html::style('css/inicioSesion.css')!!}
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('https://fonts.googleapis.com/css?family=Poppins')!!}
    {!!Html::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')!!}
    {!!Html::style('css/principal.css')!!}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <script src="https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest.js"></script>
    <link type="text/css" rel="stylesheet" href="https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest.css"/>

    <link type="text/css" rel="stylesheet" href="https://api.mqcdn.com/sdk/place-search-js/v1.0.0/place-search.css"/>
</head>
<body>
    









    @yield('seccion');




    <footer>
        <div class="container-fluid position-relative">
           <div class="row">
               <div class="col-md-8 offset-md-2 text-center">
                <h5>Proyecto de Semestre</h5>
               </div>
                 
            </div>
        </div>

    </footer>

    {!!Html::script('js/jquery.js')!!}
    <script src="js/jquery3.js"></script>
    {!!Html::script('js/bootstrap.min.js')!!}
    {!!Html::script('js/ajaxPaquete.js')!!}
    {!!Html::script('js/mapa.js')!!}
    {!!Html::script('js/agregarPaquetes.js')!!}
    {!!Html::script('js/mostrarCerrar.js')!!}


    <script src="https://api.mqcdn.com/sdk/place-search-js/v1.0.0/place-search.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
   
</body>
</html>