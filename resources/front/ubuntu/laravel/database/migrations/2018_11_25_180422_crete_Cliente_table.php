<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreteClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 
        Schema::create('clientes',function(blueprint $table){
            $table->increments('id')->unique();
            $table->integer('id_usuario')->unsigned();
            $table->string('nombre',20);
            $table->string('apellido',20);
            $table->date('fecha_nacimiento');
           
            
            $table->foreign('id_usuario')->references('id')->on('usuarios');
            
            
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('clientes');
    }
}
