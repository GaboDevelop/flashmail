<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreteDireccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('direcciones',function(blueprint $table){
            $table->increments('id')->unique();
            $table->integer('id_cliente')->unsigned();
            $table->string('pais');
            $table->string('estado',20);
            $table->string('ciudad',20);
            $table->string('codigo_postal',20);
            $table->string('zona',200);
            $table->string('zona_2',200);
            


            $table->foreign('id_cliente')->references('id')->on('clientes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('direcciones');
    }
}
