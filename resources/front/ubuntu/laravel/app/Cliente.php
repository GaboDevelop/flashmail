<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table = "clientes";

    protected $fillable = ['nombre','apellido','fecha_nacimiento','id_usuario'];
    
    
    public function usuario(){
        return $this->hasOne('App\Usuario');
    }

    public function direccion(){
        return $this->belongsTo('App\Direccion');
    }
}
