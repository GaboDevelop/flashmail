<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    //
    protected $table = 'direcciones';

    protected $fillable = ['id_cliente','pais','estado','ciudad','codigo_postal','zona','zona_2'];

    public function cliente(){
        return $this->hasOne('App\Cliente');
    }
}
