
var L;
var numeroMarcadores = 0;
var primeraVez = true;

L.mapquest.key = 'E9EKSMkLWxbRBE4NYn9P3GVHAnAmbbjd';
var popup = L.popup();


var map = L.mapquest.map('map', {
    center: [10.501823, -66.912105],
    layers: L.mapquest.tileLayer('map'),
    zoom: 12
});

map.addControl(L.mapquest.control());


function colocarMarcador(ev) {
    if (numeroMarcadores == 2){
        alert('ya marcaste 2');
    }else {
        if (numeroMarcadores == 0){
            marcador1 = L.marker(ev.latlng);
            marcador1.addTo(map);
        }else{
            marcador2 = L.marker(ev.latlng);
            marcador2.addTo(map);
        }
        popup.setLatLng(ev.latlng).openOn(map);
        L.mapquest.geocoding().reverse(ev.latlng, generatePopupContent);
        numeroMarcadores++;
        if (numeroMarcadores == 2){
            primerVez = false;
            mostrarPedido('.paquete',false);
            document.getElementById('menuLateral').style.visibility = 'visible';
        }
    }
}


function generatePopupContent(error, response) {

    
    var location = response.results[0].locations[0];
    var calle = location.street;
    var ciudad = location.adminArea5;
    var estado = location.adminArea3;
    var pais = location.adminArea1;
    var codigoPostal = location.postalCode;
    popup.setContent(calle + ' ' + ciudad + ', ' + estado + ', ' + pais+', '+codigoPostal);

    valoresDireccion(calle,ciudad,estado,pais,codigoPostal);
}

function valoresDireccion(calle,ciudad,estado,pais,codigoPostal){

    if(numeroMarcadores==1){
        document.getElementById('paisRecolectado').value = pais;
        document.getElementById('estadoRecolectado').value = estado;
        document.getElementById('ciudadRecolectado').value = ciudad;
        document.getElementById('zonaRecolectado').value = calle;
        document.getElementById('codigoPostalRecolectado').value = codigoPostal;
    }else{
        document.getElementById('paisEntrega').value = pais;
        document.getElementById('estadoEntrega').value = estado;
        document.getElementById('ciudadEntrega').value = ciudad;
        document.getElementById('zonaEntrega').value = calle;
        document.getElementById('codigoPostalEntrega').value = codigoPostal;
    }
    
}

function activarMapa(){
    document.getElementById('botonMapa').style.display = 'none';
    document.getElementById('menuLateral').style.visibility = 'hidden';
    mostrarPedido('.paquete',true);
    map.on('click',function (ev){
        colocarMarcador(ev);
    });
}




