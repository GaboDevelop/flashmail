var perfilAbierto = false;
var pedidoAbierto = false;
var accion;
$('.perfil').hide();
$('.paquete').hide();



function cambiarBool(contenedor,estado) {
    if(contenedor == ".perfil"){
        perfilAbierto = estado;
    }else if (contenedor == ".paquete"){
        pedidoAbierto = estado;
    }
}


function mostrar(contenedor) {
    if(contenedor == ".perfil"){
        accion = perfilAbierto;
    }else if(contenedor == ".paquete"){
        accion = pedidoAbierto;
    }

    if(accion){
        $(contenedor).hide("slow");
        cambiarBool(contenedor , false);
    }else {
        $(contenedor).show("slow");
        cambiarBool(contenedor, true);
    }
}


function mostrarPerfil() {
    mostrar(".perfil");
}

function mostrarPedido() {
    mostrar(".paquete");
}