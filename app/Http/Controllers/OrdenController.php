<?php

namespace flashmail\Http\Controllers;

use Illuminate\Http\Request;

use flashmail\Http\Requests;
use flashmail\Http\Controllers\Controller;
use flashmail\Paquete;
use flashmail\Orden;
use flashmail\Direccion;

class OrdenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            
            $direccionRecolectado = Direccion::create([
                'pais' => $request['paisRecolectado'],
                'estado' => $request['estadoRecolectado'],
                'ciudad' => $request['ciudadRecolectado'],
                'zona' => $request['zonaRecolectado'],
                'codigo_postal' => $request['codigoPostalRecolectado'],
                'cliente_id' => $request['clienteId']
            ]);
            
            $idDireccionRecolectado = $direccionRecolectado->id;

            $direccionEntrega = Direccion::create([
                'pais' => $request['paisEntrega'],
                'estado' => $request['estadoEntrega'],
                'ciudad' => $request['ciudadEntrega'],
                'zona' => $request['zonaEntrega'],
                'codigo_postal' => $request['codigoPostalEntrega'],
                'cliente_id' => $request['clienteId']
            ]);
            
            $idDireccionEntrega = $direccionEntrega->id;

            $orden = Orden::create([
                'nombre_destinatario' => $request['nombreDestinatario'],
                'apellido_destinatario' => $request['apellidoDestinatario'],
                'cliente_id' => $request['clienteId'],
                'direccion_entrega' => $idDireccionEntrega,
                'direccion_recolectado' => $idDireccionRecolectado
            ]);

            $idOrden = $orden->id;
            
            $contenido = $request['contenido'];
            $peso = $request['peso'];
            if(is_array($contenido)){
                $cantidad = $request['cantidadPaquetes'];
                for($i = 0;$i<$cantidad;$i++){
                    $paquete = Paquete::create([
                        'contenido' => $contenido[$i],
                        'peso' => $peso[$i],
                        'orden_id' => $idOrden
                    ]);
                }
            }else{
               return "qlossss"; 
            }
            

            return response()->json([
                'mensaje' => 'creado'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
