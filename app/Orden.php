<?php

namespace flashmail;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    protected $table = 'Ordenes';

    protected $fillable = ['direccion_entrega','direccion_recolectado', 'nombre_destinatario','apellido_destinatario','cliente_id'];
}
