<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ordenes',function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_destinatario',20);                                                                                                                                                                               
            $table->string('apellido_destinatario',20);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          


            $table->integer('cliente_id')->unsigned();
            $table->integer('direccion_entrega');
            $table->integer('direccion_recolectado');


            $table->foreign('cliente_id')->references('id')->on('Clientes');
            
          
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
